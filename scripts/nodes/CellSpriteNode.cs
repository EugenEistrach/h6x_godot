namespace h6x.scripts.nodes
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using Godot;

    using h6x.scripts.grid;

    /// <summary>
    /// The cell sprite node.
    /// </summary>
    public class CellSpriteNode : Sprite
    {
        /// <summary>
        /// The _leveled textures.
        /// </summary>
        private static List<Texture> leveledTextures;

        /// <summary>
        /// The parent cell node.
        /// </summary>
        private CellNode cellNode;

        /// <inheritdoc/>
        public override void _Ready()
        {
            this.LoadLeveledSprites(2);
            this.LoadCellNode();
        }

        /// <summary>
        /// Loads all leveled sprites
        /// </summary>
        /// <param name="maxLevel">
        /// The max Level.
        /// </param>
        private void LoadLeveledSprites(int maxLevel)
        {
            if (leveledTextures != null)
                return;
            leveledTextures = new List<Texture>();

            const string Path = "res://game/scenes/cell/";
            var regex = new Regex(@"^cell\d\.png$");
            var dir = new Directory();
            dir.Open(Path);
            dir.ListDirBegin(true, true);
            var file = "_";
            
            while (!file.Empty())
            {
                file = dir.GetNext();
                if (regex.IsMatch(file))
                {
                    leveledTextures.Add((Texture)ResourceLoader.Load($"{Path}{file}"));
                }
                    
                if (leveledTextures.Count > maxLevel)
                    break;
            }
        }

        /// <summary>
        /// Loads parent cell node.
        /// </summary>
        private void LoadCellNode()
        {
            this.cellNode = this.GetParent() as CellNode;
            if (this.cellNode != null)
            {
                this.cellNode.OnDataChange += this.Redraw;
                this.Redraw(this.cellNode.Data);
            }
        }

        /// <summary>
        /// Redraws the cell.
        /// </summary>
        /// <param name="data">
        /// The cell data.
        /// </param>
        private void Redraw(CellData data)
        {
            if (data?.ExpandedData == null)
                return;

            var level = data.ExpandedData.Level;
            
            if (level >= 0 && level < leveledTextures.Count)
            {
                this.Texture = leveledTextures[level];
            }
        }
    }
}
