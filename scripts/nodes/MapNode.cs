namespace h6x.scripts.nodes
{
    using Godot;

    using h6x.scripts.grid;
    using h6x.scripts.grid.enums;

    using Newtonsoft.Json;

    /// <summary>
    /// The map generator.
    /// </summary>
    public class MapNode : Node
    {
        /// <summary>
        /// The cell scene.
        /// </summary>
        [Export]
        private PackedScene cellScene;

        /// <summary>
        /// The hex map.
        /// </summary>
        private HexMap hexMap;

        /// <inheritdoc/>
        public override void _Ready()
        {
            this.GenerateMap();
        }

        /// <summary>
        /// Generate the map.
        /// </summary>
        public void GenerateMap()
        {
            var mapJson = this.GetFirstMap();
            if (string.IsNullOrEmpty(mapJson))
                return;
            this.hexMap = new HexMap(mapJson, 256, 8);

            foreach (var cellData in this.hexMap)
            {
                if (cellData.CellType == CellType.None)
                    continue;
                var cell = (CellNode)this.cellScene.Instance();
                this.AddChild(cell);
                cell.Position = this.hexMap.PointToScreen(cellData.Point);
                cell.Setup(cellData);
            }
        }

        /// <summary>
        /// Get first map.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetFirstMap()
        {
            var directory = new Directory();

            if (directory.Open("res://game/maps/") == Error.Failed)
                return string.Empty;           

            directory.ListDirBegin();
            var fileName = directory.GetNext();

            while (!string.IsNullOrEmpty(fileName))
            {
                if (fileName.EndsWith(".json"))
                    break;
                fileName = directory.GetNext();
            }

            var f = new File();
            f.Open($"res://game/maps/{fileName}", (int)File.ModeFlags.Read);
            var json = f.GetAsText();

            return json;
        }
    }
}
