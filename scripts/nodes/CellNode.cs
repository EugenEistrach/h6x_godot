namespace h6x.scripts.nodes
{
    using System;

    using Godot;

    using h6x.scripts.grid;

    using Object = Godot.Object;

    /// <summary>
    /// The cell node.
    /// </summary>
    public class CellNode : Area2D
    {
        /// <summary>
        /// Event that gets triggered when cell data changes.
        /// </summary>
        public event Action<CellData> OnDataChange;

        /// <summary>
        /// Gets the cell data.
        /// </summary>
        public CellData Data { get; private set; }

        /// <inheritdoc/>
        public override void _Ready()
        {
            this.LoadDefaultData();
        }

        /// <summary>
        /// Setups the cell.
        /// </summary>
        /// <param name="d">
        /// The cell data.
        /// </param>
        public void Setup(CellData d)
        {
            this.Data = d;
            this.TriggerOnDataChange();
        }

        /// <summary>
        /// Updates cell data with new data
        /// </summary>
        /// <param name="data">
        /// The cell data.
        /// </param>
        public void UpdateData(CellData data)
        {
            if (data.Point != this.Data.Point)
                return;
            this.Data = data;
            this.TriggerOnDataChange();
        }

        /// <summary>
        /// Loads default data if no data was setup.
        /// </summary>
        private void LoadDefaultData()
        {
            if (this.Data != null)
                return;
            this.Data = new CellData
                            {
                                Point = new Point(0, 0),
                                ExpandedData = new ExpandedCellData
                                                   {
                                                       Level = 1,
                                                       Units = 3,
                                                   }
                            };
            this.TriggerOnDataChange();
        }

        /// <summary>
        /// Triggers the OnDataChange event
        /// </summary>
        private void TriggerOnDataChange()
        {
            this.OnDataChange?.Invoke(this.Data);
        }

        /// <summary>
        /// The on cell input signal callback.
        /// </summary>
        /// <param name="viewport">
        /// The viewport.
        /// </param>
        /// <param name="evt">
        /// The event.
        /// </param>
        /// <param name="shapeIdx">
        /// The shape_idx.
        /// </param>
        private void OnCellInput(Object viewport, Object evt, int shapeIdx)
        {
            switch (evt)
            {
                case InputEventMouseButton emb:
                    this.HandlePress();
                    break;
            }
        }

        /// <summary>
        /// Handles mouse or touch press
        /// </summary>
        private void HandlePress()
        {
            return;
        }   
    }
}
