﻿namespace h6x.scripts.grid
{
    using System;

    /// <summary>
    /// The map data used to serialize from and to json.
    /// </summary>
    [Serializable]
    public class MapData
    {
        /// <summary>
        /// Gets or sets the cells.
        /// </summary>
        public CellData[] Cells { get; set; }
    }
}