﻿namespace h6x.scripts.grid
{
    using System.Collections;
    using System.Collections.Generic;

    using Godot;

    using Newtonsoft.Json;

    /// <summary>
    /// The hex map.
    /// </summary>
    public class HexMap : IEnumerable<CellData> 
    {
        /// <summary>
        /// The cells.
        /// </summary>
        private readonly Dictionary<Point, CellData> cells = new Dictionary<Point, CellData>();

        /// <summary>
        /// The cell size in pixel.
        /// </summary>
        private int cellSize;

        /// <summary>
        /// The padding.
        /// </summary>
        private int padding;

        /// <summary>
        /// Initializes a new instance of the <see cref="HexMap"/> class.
        /// </summary>
        /// <param name="json">
        /// The json string.
        /// </param>
        /// <param name="cellSize">
        /// Cell size in pixel
        /// </param>
        /// <param name="padding">
        /// Padding in pixel
        /// </param>
        public HexMap(string json, int cellSize, int padding)
        {
            this.padding = padding;
            var mapData = JsonConvert.DeserializeObject<MapData>(json);
            this.BuildMap(mapData, cellSize);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HexMap"/> class.
        /// </summary>
        /// <param name="data">
        /// Map data.
        /// </param>
        /// <param name="cellSize">
        /// Cell size in pixel
        /// </param>
        /// <param name="padding">
        /// Padding in pixel
        /// </param>
        public HexMap(MapData data, int cellSize, int padding)
        {
            this.padding = padding;
            this.BuildMap(data, cellSize);
        }

        /// <summary>
        /// Gets the east offset.
        /// </summary>
        public static Point East { get; } = new Point(1, 0);

        /// <summary>
        /// Gets the north east offset.
        /// </summary>
        public static Point NorthEast { get; } = new Point(0, 1);

        /// <summary>
        /// Gets the north west offset.
        /// </summary>
        public static Point NorthWest { get; } = new Point(-1, 1);

        /// <summary>
        /// Gets the west offset.
        /// </summary>
        public static Point West { get; } = new Point(-1, 0);

        /// <summary>
        /// Gets the south west offset.
        /// </summary>
        public static Point SouthWest { get; } = new Point(0, -1);

        /// <summary>
        /// Gets the south east offset.
        /// </summary>
        public static Point SouthEast { get; } = new Point(1, -1);

        /// <summary>
        /// Gets The directions.
        /// </summary>
        public static Point[] Directions { get; } =
                {
                    East,
                    NorthEast,
                    NorthWest,
                    West,
                    SouthWest,
                    SouthEast
                };

        /// <summary>
        /// Returns a cell for a given point.
        /// </summary>
        /// <param name="p">
        /// The point.
        /// </param>
        /// <returns>
        /// The <see cref="Cell"/>.
        /// </returns>
        public CellData this[Point p] => this.cells[p];

        /// <summary>
        /// Get the position in screen coordinates.
        /// </summary>
        /// <param name="p">
        /// The point.
        /// </param>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        public Vector2 PointToScreen(Point p)
        {
            return new Vector2
                       {
                           x = (this.cellSize + this.padding) * Mathf.Sqrt(3f) * (p.X + p.Y / 2f),
                           y = (this.cellSize + this.padding) * (3f / 2) * p.Y
                       };
        }

        /// <inheritdoc />
        public IEnumerator<CellData> GetEnumerator()
        {
            return this.cells.Values.GetEnumerator();
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Builds the map.
        /// </summary>
        /// <param name="data">
        /// Map data.
        /// </param>
        /// <param name="cs">
        /// Cell size in pixel.
        /// </param>
        private void BuildMap(MapData data, int cs)
        {
            this.cellSize = cs / 2;
            foreach (var cell in data.Cells)
            {
                this.cells[cell.Point] = cell;
            }
        }       
    }
} 