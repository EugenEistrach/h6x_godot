﻿namespace h6x.scripts.grid
{
    using System;

    using h6x.scripts.grid.enums;

    /// <summary>
    /// The cell data.
    /// </summary>
    [Serializable]
    public class CellData
    {
        /// <summary>
        /// Gets or sets the point.
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// Gets or sets the cell type.
        /// </summary>
        public CellType CellType { get; set; }

        /// <summary>
        /// Gets or sets the buff type.
        /// </summary>
        public BuffType BuffType { get; set; }

        /// <summary>
        /// Gets or sets the owner id.
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        /// Gets or sets the expanded data.
        /// </summary>
        public ExpandedCellData ExpandedData { get; set; }
    }
}