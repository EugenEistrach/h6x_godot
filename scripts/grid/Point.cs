﻿namespace h6x.scripts.grid
{
    using System;

    /// <summary>
    /// The point.
    /// </summary>
    [Serializable]
    public class Point : IEquatable<Point>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> class.
        /// </summary>
        /// <param name="x">
        /// The x coordinate.
        /// </param>
        /// <param name="y">
        /// The y coordinate.
        /// </param>
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
            this.Z = -this.X - this.Y;
        }

        /// <summary>
        /// Gets the x coordinate.
        /// </summary>
        public int X { get; }

        /// <summary>
        /// Gets the y coordinate.
        /// </summary>
        public int Y { get; }

        /// <summary>
        /// Gets the z coordinate.
        /// </summary>
        public int Z { get; }

        /// <summary>
        /// The + overload.
        /// </summary>
        /// <param name="p1">
        /// The p 1.
        /// </param>
        /// <param name="p2">
        /// The p 2.
        /// </param>
        /// <returns>
        /// The <see cref="Point"/>
        /// </returns>
        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        /// <summary>
        /// The - overload.
        /// </summary>
        /// <param name="p1">
        /// The p 1.
        /// </param>
        /// <param name="p2">
        /// The p 2.
        /// </param>
        /// <returns>
        /// The <see cref="Point"/> 
        /// </returns>
        public static Point operator -(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p1.Y - p2.Y);
        }

        /// <summary>
        /// The == overload.
        /// </summary>
        /// <param name="p1">
        /// The p 1.
        /// </param>
        /// <param name="p2">
        /// The p 2.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool operator ==(Point p1, Point p2)
        {
            return Equals(p1, p2);
        }

        /// <summary>
        /// The != overload.
        /// </summary>
        /// <param name="p1">
        /// The p 1.
        /// </param>
        /// <param name="p2">
        /// The p 2.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool operator !=(Point p1, Point p2)
        {
            return !(p1 == p2);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && this.Equals((Point)obj);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            var hash = 17;
            hash = (hash * 31) + this.X;
            hash = (hash * 31) + this.Y;
            return hash;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"X: {this.X}, Y: {this.Y}, Z:{this.Z}";
        }

        /// <inheritdoc />
        public bool Equals(Point obj)
        {
            if (obj == null)
                return false;

            return this.X == obj.X && this.Y == obj.Y;
        }  
    }
}
