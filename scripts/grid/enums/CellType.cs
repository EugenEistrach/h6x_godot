﻿namespace h6x.scripts.grid.enums
{
    /// <summary>
    /// The cell type.
    /// </summary>
    public enum CellType
    {
        /// <summary>
        /// No cell.
        /// </summary>
        None,

        /// <summary>
        /// Player cell.
        /// </summary>
        Player,

        /// <summary>
        /// Buff cell.
        /// </summary>
        Buff
    }
}