﻿namespace h6x.scripts.grid.enums
{
    /// <summary>
    /// The buff type.
    /// </summary>
    public enum BuffType
    {
        /// <summary>
        /// No buff.
        /// </summary>
        None,

        /// <summary>
        /// Defensive buff.
        /// </summary>
        Def,

        /// <summary>
        /// Offensive buff.
        /// </summary>
        Atk,

        /// <summary>
        /// Auto power increase buff.
        /// </summary>
        AutomaticPowerIncrease,

        /// <summary>
        /// Extra coins buff.
        /// </summary>
        ExtraCoins
    }
}