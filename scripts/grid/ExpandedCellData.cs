﻿namespace h6x.scripts.grid
{
    using System;

    /// <summary>
    /// The expanded cell data.
    /// </summary>
    [Serializable]
    public class ExpandedCellData
    {
        /// <summary>
        /// Gets or sets the units.
        /// </summary>
        public int Units { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        public int Level { get; set; }
    }
}